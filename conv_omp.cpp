#include <iostream>
#include <sstream>
#include <stdlib.h> 
#include <omp.h>
#include <stdio.h>

using namespace std;

int nths;
int n_ke = 5;

int n_im = 2000;
int ksize = n_ke*n_ke;
int n_ke_m = n_ke/2;

int **ke = new int*[n_ke];
int **im = new int*[n_im];
int **re = new int*[n_im];


float conv1(int ind_i, int ind_j, int ti, int tj, int sum);
float conv2(int ind_i, int ind_j, int ti, int tj, int sum);


int main(int argc, char *argv[])
{
    int sum;
	int i,j;	


	if (argc!=2){
		try{throw -1;}
		catch(int){
			cerr<<"argc!"<<endl;
			return 0;
		}
	}

	istringstream iss(argv[1]);
	if (!(iss >> nths)){
		try{throw -1;}
		catch(int){
			cerr<<"Invalid number!"<<endl;
		}
	}

	for (i = 0; i < n_ke; i++)
	{
		ke[i] = new int[n_ke];
	}

	for (i = 0; i < n_im; i++)
	{
		im[i] = new int[n_im];
		re[i] = new int[n_im];
	}

	for (i = 0; i < n_ke; i++)
	{
		for (j = 0; j < n_ke; j++)
		{
			ke[i][j] = 1;
		}
	}

	for (i = 0; i < n_im; i++)
	{
		for (j = 0; j < n_im; j++)
		{
			im[i][j] = 1;
		}
	}

	for (i = 0; i < n_im; i++)
	{
		for (j = 0; j < n_im; j++)
		{
			re[i][j] = -1;
		}
	}

	cout<<"number of threads: "<<nths<<endl;
	double wtime1;
    double wtime1f;
    int ti, tj;

//  Convolucion
    wtime1 = omp_get_wtime();  
 
#	pragma omp parallel num_threads(nths) \
	default(none) private(i,j,sum,ti,tj) shared(im,re,ke,n_im,n_ke_m)

#	pragma omp for schedule(dynamic,1)
    for (i = n_ke_m; i < n_im-n_ke_m; i++){
		for (j = n_ke_m; j < n_im-n_ke_m; j++){
            re[i][j] = conv1(i,j,ti,tj,sum);
		}
	}

#   pragma omp for schedule(dynamic,1)
    for (i = 0; i<n_im; i++){
        for (j = 0; j<n_ke_m; j++){
            re[i][j] = conv2(i,j,ti,tj,sum);
        }
    }

#   pragma omp for schedule(dynamic,1)
    for (i = 0; i<n_im; i++){
        for (j = n_im-n_ke_m;j<n_im; j++){
             re[i][j] = conv2(i,j,ti,tj,sum);
        }
    }

#   pragma omp for schedule(dynamic,1)
    for (j = n_ke_m; j<n_im-n_ke_m; j++){
        for (i = 0; i<n_ke_m; i++){
            re[i][j] = conv2(i,j,ti,tj,sum);
        }
    }

#   pragma omp for schedule(dynamic,1)
    for (j = n_ke_m; j<n_im-n_ke_m; j++){
        for (i = n_im-n_ke_m; i<n_im; i++){
             re[i][j] = conv2(i,j,ti,tj,sum);
        }
    }

	wtime1f = omp_get_wtime();
	printf("\n\ttime(parallel)\t: \t%.6lfs\n", (wtime1f-wtime1));
    wtime1 = wtime1f - wtime1;     

//  checkout
    int bugs = 0;
    for (i = 0; i < n_im; i++){
        for (j = 0; j < n_im; j++){
            if (re[i][j]!=1){
                cout<<"bug in  "<<i<<", "<<j<<" = "<<re[i][j]<<endl;
                bugs++;
            }
        }
    }

    cout<<"bugs= "<<bugs<<endl;

    double wtime = omp_get_wtime();
	for (i = 0; i < n_im; i++){
		for (j = 0; j < n_im; j++){
			if (i < n_ke_m || j < n_ke_m || i>=n_im-n_ke_m || j>=n_im-n_ke_m){
                re[i][j] = conv2(i,j, ti, tj, sum);
            }
            else {
                re[i][j] = conv1(i,j,ti, tj, sum);
            }
		}
	}

	double wtime2 = omp_get_wtime() - wtime;
	printf("\ttime(serial)\t: \t%.6lfs\n", wtime2);	

//  checkout
    bugs = 0;
    for (i = 0; i < n_im; i++){
        for (j = 0; j < n_im; j++){
            if (re[i][j]!=1){
                cout<<"bug in "<<i<<", "<<j<<" = "<<re[i][j]<<endl;
                bugs++;
            }
        }
    }

    cout<<"bugs= "<<bugs<<endl;
	printf("\nspeedup: %lf, efi: %lf\n",
                wtime2/wtime1,wtime2/(wtime1*double(nths)));

//  delete
	for (i = 0; i < n_im; i++)
	{
		delete [] im[i];
		delete [] re[i];
	}

	delete [] im;
	delete [] re;

	for (i = 0; i < n_ke; i++)
	{
		delete [] ke[i];
	}
	delete [] ke;

	return 0;
}

float conv1(int ind_i, int ind_j, int ti, int tj, int sum)
{
	sum = 0;
    for (ti = ind_i-n_ke_m; ti < ind_i+n_ke_m+1; ti++){
        for (tj = ind_j-n_ke_m; tj < ind_j+n_ke_m+1; tj++){
                sum+=im[ti][tj]*ke[ti-ind_i+n_ke_m][tj-ind_j+n_ke_m];
        }
	}

	return sum/ksize;
}

float conv2(int ind_i, int ind_j, int ti, int tj, int sum)
{
	sum = 0;
    int count = 0;
    for (ti = ind_i-n_ke_m; ti < ind_i+n_ke_m+1; ti++){
        if (ti>=0 && ti<n_im){
            for (tj = ind_j-n_ke_m; tj < ind_j+n_ke_m+1; tj++){
                if (tj>=0 && tj<n_im){
                    sum+=im[ti][tj]*ke[ti-ind_i+n_ke_m][tj-ind_j+n_ke_m];
                    count++;
                }
		    }
        }
	}
	return sum/count;
}
